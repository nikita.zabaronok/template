$(function () {
	'use strict'
	$(".owl-carousel").owlCarousel({
		items: 1,
		loop: true,
		dots: true,
		nav: true,
		navText: ["",""],
		//navContainerClass: 'container owl-nav',
		//autoplay: true,
		autoplayTimeout: 5000
	});
});
$(document).ready(function () {
	$('[data-toggle="offcanvas"]').click(function () {
	  $('.row-offcanvas').toggleClass('active')
	});
});

//catalog
$(document).ready(function () {
	$('.list-group .list-group-item .dropdown-arrow').click(function() {
		//$('.list-group .list-group-item').toggleClass('active');
		$(this).parent().toggleClass('active');
	});
});

$(document).ready(function () {
	$('.list-group .list-group-item li').click(function() {
		//$('.list-group .list-group-item').toggleClass('active');
		$(this).toggleClass('bolden');
	});
});

//
$(document).ready(function () {
	$('.rules-tabs .nav-item').click(function() {
		$('.rules-tabs .nav-item').removeClass('active');
		$(this).toggleClass('active');
	});
});

$(document).ready(function () {
	$('.nav-pills .nav-item').click(function() {
		$('.nav-pills .nav-item').removeClass('active');
		$(this).toggleClass('active');
	});
});

//order
const elem = document.querySelector('#number');
const totalCost = document.querySelector('#total');
document.addEventListener('click', function (e) {
	curValue = parseInt(elem.textContent);
	curTotal = 4760;
	if (e.target.classList.contains("plus")) {
		elem.innerHTML = ++curValue;
		totalCost.innerHTML = curTotal * curValue;
	} else if (e.target.classList.contains("minus")) {
		if(curValue >= 1) {
			elem.innerHTML = --curValue;
			totalCost.innerHTML = curTotal * curValue;
		}
	}
});
